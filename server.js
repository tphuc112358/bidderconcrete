/*
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

// Create connection to database
var config = 
{
     userName: 'tphuc112358', // update me
     password: 'Nc2kld2fs2f', // update me
     server: 'tphuc112358.database.windows.net', // update me
     options: 
     {
           database: 'test' //update me
           , encrypt: true
       }
   }
   var connection = new Connection(config);

// Attempt to connect and execute queries if connection goes through
connection.on('connect', function(err) {
	if (err) 	{
		console.log(err)
	}
	else	{
		queryDatabase()
	}
});

function queryDatabase() { 
    console.log('Reading rowsls
     from the Table...');

    // Read all rows from table
    request = new Request(
    	"select * from test",
    	function(err, rowCount, rows) 
    	{
    		console.log(rowCount + ' row(s) returned');
    		process.exit();
    	}
    	);

    request.on('row', function(columns) {
    	columns.forEach(function(column) {
    		console.log("%s\t%s", column.metadata.colName, column.value);
    	});
    });
    connection.execSql(request);
}
*/
var express = require('express');
var app = express();
var port = process.env.PORT || 1337;

mssql = require('mssql'),
db = require('./api/models/todoListModel');
bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route


app.listen(port, function() {
	console.log('to do list RESTful API server started on: ' + port);
});

